-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: formula1_review
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `circuito`
--

DROP TABLE IF EXISTS `circuito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `circuito` (
  `ID_CIRCUITO` int NOT NULL AUTO_INCREMENT,
  `NOMBRE_CIRCUITO` varchar(30) NOT NULL,
  `PAIS_CIRCUITO` varchar(20) DEFAULT NULL,
  `CIUDAD_CIRCUITO` varchar(20) DEFAULT NULL,
  `NUMERO_VUELTAS_CIRCUITO` int DEFAULT NULL,
  `LONGITUD_VUELTA` float DEFAULT NULL,
  `NUMERO_CURVAS_VUELTA` int DEFAULT NULL,
  PRIMARY KEY (`ID_CIRCUITO`),
  UNIQUE KEY `NOMBRE_CIRCUITO` (`NOMBRE_CIRCUITO`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `circuito`
--

LOCK TABLES `circuito` WRITE;
/*!40000 ALTER TABLE `circuito` DISABLE KEYS */;
INSERT INTO `circuito` VALUES (1,'Circuito de Spa-Francorchamps','Francorchamps','Bélgica',44,7004,19),(2,'Circuito de Monza','Monza','Italia',53,5793,11),(3,'Circuito de Suzuka','Suzuka','Japón',53,5807,18),(4,'Circuito de Sakhir','Sakhir','Baréin',57,5412,15),(5,'Circuito de Melbourne','Melbourne','Australia',58,5303,16),(6,'Circuito de Gilles Villeneuve','Montreal','Canadá',70,4361,14),(7,'Circuito de Istanbul Park','Pendik','Estambúl',58,5338,14),(8,'Circuito de Sepang','Sepang','Malasia',56,5543,15),(9,'Circuito de Interlagos','Sao Paulo','Brasil',71,4309,15),(10,'Circuito Montmelo','Barcelona','España',56,5451,16),(11,'Circuito de Nurburgring','Nürburg','Alemania',60,5148,15),(23,'Circuito del Jarama','España','Madrid',40,4004,15),(24,'Circuito Liga Norte','Italia','Sicilia',89,3003,18),(25,'nombreCircuito','paisCircuito','ciudadCircuito',90,4000,20),(26,'khg','Ã±kjlk','bnvmv',121,4000,20);
/*!40000 ALTER TABLE `circuito` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-24  0:38:13
