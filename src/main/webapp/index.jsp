<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML>

<html>
    <head>
        <title>KERS-CIRCUITOS</title>
        <meta charset = "utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen" />
    </head>
    <body>
        <div id="contenedor" class="flex-container">

            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <%@include file="/WEB-INF/jspf/navegador.jspf" %>

            <section class="flex-item">

                <p><em><strong>E</strong></em>l sistema KERS  se ha implantado
                    en los monoplazas de formula 1, pero no es obligatorio. Este
                    sistema permite recuperar la energía que se genera en las frenadas
                    y la emplea durante el circuito por algunos segundos.
                </p>
                <p><em><strong>L</strong></em>a energía se genera en las frenadas,
                    se almacenan y luego puede ser empleada en diferentes situaciones
                    de carrera al pulsar un botón.  El monoplaza se vuelve más potente
                    durante unos segundos y esto puedes ser aprovechado en las rectas,
                    pasos de curva o en adelantamientos.
                </p>                
                <p><em><strong>E</strong></em>l sistema KERS puede ser eléctrico como
                    el que hemos venido mencionando, mecánico (a través de volante de
                    inercia) o neumático (activación neumática). En cualquiera de estos
                    dura tan solo unos segundos, entre 6 y 7 y puede generar entre 70 y 80 HP.
                </p>
                <p><em><strong>E</strong></em>n la actualidad los reglamentos permiten a los
                    sistemas un máximo de 80 BHP (Bracke Horse Power)que quedan disponibles para
                    cualquier situación de carrera durante 6 segundos por vuelta.
                    Según la regulación debe ser de activación bajo el control del piloto.
                </p>
                <cite>Sistema Kers en la Formula, publicación: Prueba de ruta. Autor: Gilbert Mauricio García Orozco</cite>
            </section>
            <%@ include file="/WEB-INF/jspf/footer.jspf" %>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>
</html>