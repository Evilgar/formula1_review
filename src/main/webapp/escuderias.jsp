<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



<!DOCTYPE HTML>

<html>
    <head>
        <title>KERS-CIRCUITOS</title>
        <meta charset = "utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen" />
    </head>
    <body>
        <div id="contenedor" class="flex-container">

            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <%@include file="/WEB-INF/jspf/navegador.jspf" %>
            <section id="portada">

                <h1>Escuderías de Fórmula 1</h1>

                <div class="txt">
                    <p>     Se conoce como escudería al conjunto de personas y objetos
                        que forman un equipo conjunto dentro de algún deporte de motor,
                        sea rallyes, Fórmula 1, motociclismo, etc, y que está formado por
                        un conjunto de vehículos y pilotos. También se puede reducir esta
                        definición al conjunto de automóviles de un mismo equipo de
                        competición. 1​ Según la definición italiana, idioma desde donde
                        proviene el término, una escudería es una organización deportiva
                        que alberga a los autos de competición que compiten en la misma
                        casa y proporcionan asistencia técnica a los pilotos.
                    </p>
                </div>

                <div class="card-deck">
                    <div class="card">
                        <img src="./imagenes/e_alfa_romeo.jpg"
                             alt="Alfa Romeo Racing" width="135" height="90">
                        <div class="card-body">
                            <h4 class="card-title">Alfa Romeo Racing</h4>
                        </div>
                        <div class="card-footer">
                            <a  href="https://es.wikipedia.org/wiki/Alfa_Romeo_en_F%C3%B3rmula_1" class="btn btn-light"
                                target="_blank">Ver en Wikipedia</a> 
                        </div>
                    </div>
                    <div class="card">
                        <img src="./imagenes/e_ferrari.jpg" alt="Ferrari" width="135"
                             height="90">
                        <div class="card-body">
                            <h4 class="card-title">Ferrari</h4>
                        </div>
                        <div class="card-footer">
                            <a href="https://es.wikipedia.org/wiki/Ferrari" class="btn btn-light" 
                               target="_blank">Ver en Wikipedia</a>
                        </div>
                    </div>
                    <div class="card">
                        <img src="./imagenes/e_haas.jpg"
                             alt="Haas F1 Team" width="135" height="90">
                        <div class="card-body">
                            <h4 class="card-title">Haas F1 Team</h4>
                        </div>
                        <div class="card-footer">
                            <a href="https://es.wikipedia.org/wiki/Haas_F1_Team" class="btn btn-light"  
                               target="_blank">Ver en Wikipedia</a>
                        </div>
                    </div>

                </div>

                <div class="card-deck">
                    <div class="card">
                        <img src="./imagenes/e_mclaren.jpg"
                             alt="McLaren" width="135" height="90">
                        <div class="card-body">
                            <h4 class="card-title">McLaren</h4>
                        </div>
                        <div class="card-footer">
                            <a href="https://es.wikipedia.org/wiki/McLaren_Automotive"
                               class="btn btn-light" 
                               target="_blank">Ver en Wikipedia</a>
                        </div>
                    </div>
                    <div class="card">
                        <img src="./imagenes/e_mercedes.jpg"
                             alt="Mercedes" width="135" height="90">
                        <div class="card-body">
                            <h4 class="card-title">Mercedes</h4>
                        </div>
                        <div class="card-footer">
                            <a href="https://es.wikipedia.org/wiki/Mercedes-Benz_en_F%C3%B3rmula_1"
                               target="_blank" class="btn btn-light">Ver en Wikipedia</a>
                        </div>
                    </div>
                    <div class="card">
                        <img src="./imagenes/e_racing.jpg"
                             alt="Racing Point" width="135" height="90">
                        <div class="card-body">
                            <h4 class="card-title">Racing Point</h4>
                        </div>
                        <div class="card-footer">
                            <a href="https://es.wikipedia.org/wiki/Racing_Point_F1_Team"
                               target="_blank" class="btn btn-light">Ver en Wikipedia</a>
                        </div>
                    </div>
                </div>

                <div class="card-deck">
                    <div class="card">
                        <img src="./imagenes/e_red bull.jpg"
                             alt="Red Bull Racing" width="135" height="90">
                        <div class="card-body">
                            <h4 class="card-title">Red Bull Racing</h4>
                        </div>
                        <div class="card-footer">
                            <a href="https://es.wikipedia.org/wiki/Red_Bull_Racing"
                               target="_blank" class="btn btn-light">Ver en Wikipedia</a>
                        </div>
                    </div>
                    <div class="card">
                        <img src="./imagenes/e_renault.jpg"
                             alt="Renault" width="135" height="90">
                        <div class="card-body">
                            <h4 class="card-title">Renault</h4>
                        </div>
                        <div class="card-footer">
                            <a href="https://es.wikipedia.org/wiki/Renault_en_F%C3%B3rmula_1" target="_blank"
                               class="btn btn-light">Ver en Wikipedia</a>
                        </div>
                    </div>
                    <div class="card">
                        <img src="./imagenes/e_toro rosso.jpg"
                             alt="Alpha Tauri" width="135" height="90">

                        <div class="card-body">
                            <h4 class="card-title">Alpha Tauri</h4>
                        </div>
                        <div class="card-footer">
                            <a href="https://es.wikipedia.org/wiki/Scuderia_AlphaTauri"
                               target="_blank" class="btn btn-light">Ver en Wikipedia</a>
                        </div>
                    </div>
                </div>
            </section>
            <footer> <%@include file="/WEB-INF/jspf/footer.jspf" %> </footer>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>
</html>
