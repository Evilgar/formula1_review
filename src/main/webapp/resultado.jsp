<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML>

<html>
    <head>
        <title>KERS-CIRCUITOS</title>
        <meta charset = "utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen" />
    </head>
    <body>
        <div id="contenedor" class="flex-container">

            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <%@include file="/WEB-INF/jspf/navegador.jspf" %>


            <div class="row">
                <h5>Detalles del vehículo</h5>
                <table class="table">
                    <tbody class="tbody-light">                                
                        <tr>
                            <th scope="row">Modelo</th>
                            <th scope="row">Ganancia de Potencia (KW)</th>
                            <th scope="row">Escudería</th>
                        </tr>
                        <tr>
                            <td><%=request.getAttribute("modeloCoche")%></td>
                            <td><%=request.getAttribute("gananciaPotenciaPorCurva")%></td>
                            <td><%=request.getAttribute("nombreEscuderia")%></td>
                        </tr>
                        <tr>
                            <th scope="row">Circuito:</th>
                            <td><%=request.getAttribute("nombreCircuito")%></td>
                            <td></td>
                        </tr>                           
                        <tr>
                            <th scope="row">incremento de ganancia de potencia (KW):</th>
                            <td><%=request.getAttribute("gananciaCocheSeleccionadoCircuitoSeleccionado")%></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>              
            <%@include file="/WEB-INF/jspf/footer.jspf" %>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>
</html>
