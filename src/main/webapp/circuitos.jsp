<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>


<!DOCTYPE HTML>
<html>
    <head>
        <title>KERS-CIRCUITOS</title>
        <meta charset = "utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen" />
        <script type='text/javascript'>
            function validar() {
                var isValido = true;
                var regexNombre = /^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/;
                var regexEmail = /^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/;
                var texto = [];
                const text1 = document.getElementById("nombreUsuario").value;
                const text2 = document.getElementById("apellidosUsuario").value;
                const text3 = document.getElementById("nombreCircuito").value;
                const text4 = document.getElementById("paisCircuito").value;
                const text5 = document.getElementById("ciudadCircuito").value;
                texto.push(text1);
                texto.push(text2);
                texto.push(text3);
                texto.push(text4);
                texto.push(text5);
                for (var i = 0; i < texto.length; i++) {
                    if (texto[i] == null || texto[i].length == 0 || !isNaN(texto[i]) || !(texto[i].match(regexNombre))) {
                        alert(texto[i] + ' no es un formato correcto para este campo');
                        isValido = false;
                    }
                }

                const email = document.getElementById("email").value;
                if (!email.match(regexEmail)) {
                    alert('El email no es correcto');
                    isValido = false;
                }

                var num = [];
                const numero1 = document.getElementById("numeroVueltasCircuito").value;
                const numero2 = document.getElementById("numeroCurvasVuelta").value;
                const numero3 = document.getElementById("longitudVuelta").value;
                num.push(numero1);
                num.push(numero2);
                num.push(numero3);
                for (var j = 0; j < num.length; j++) {
                    if (num[j] == null || num[j].length == 0 || isNaN(num[j])) {
                        alert('El campo no puede enviarse vacío y su valor debe ser numérico');
                        isValido = false;
                    }
                    if (num[j].equals(numero1)) {
                        if (!Number.isInteger(num[j]) || 80 < num[j] < 40) {
                            alert('El número de vueltas no puede ser inferior a 40 o ni superior a 80');
                            isValido = false;
                        }
                    }
                    if (num[j].equals(numero2)) {
                        if (!Number.isInteger(num[j]) || 20 < num[j] < 6) {
                            alert('El número de curvas no puede ser inferior a 6 o ni superior a 20');
                            isValido = false;
                        }
                    }
                    if (num[j].equals(numero3)) {
                        if (num[j] % 1 == 0 || 3000 < num[j] < 9000) {
                            alert('la longitud no puede ser inferior a 3000 o ni superior a 9000');
                            isValido = false;
                        }
                    }
                }
                return isValido;
            }
            const myForm = document.getElementById('myForm');

            myForm.addEventListener('submit', (event) => {
                if (!validar()) {
                    event.preventDefault();
                }
            });
        </script>
    </head>
    <body>
        <div id="contenedor" class="flex-container">

            <%@include file="/WEB-INF/jspf/header.jspf" %>
            <%@include file="/WEB-INF/jspf/navegador.jspf" %>
            <form id ="myForm" name="myForm" action="IncrementoGananciaPotenciaServlet" method = "post" onsubmit="return validar();" >
                <br>
                <fieldset>
                    <legend><em>Datos del usuario</em></legend>
                    <br>
                    <div class="row">					
                        <label for="nombreUsuario">Nombre: &nbsp;</label>
                        <input type = "text" id="nombreUsuario" name = "nombreUsuario" maxlength="100" required>
                    </div>                    
                    <div class="row">	
                        <label for="apellidosUsuario">Apellidos: &nbsp;</label>
                        <input type = "text" id="apellidosUsuario" name = "apellidosUsuario" maxlength="100" required>
                    </div>                    
                    <div class="row">	
                        <label for="email">Email: &nbsp;</label>
                        <input type = "email" id="email" name = "email" maxlength="50" required>
                    </div>
                </fieldset>                    
                <br>
                <fieldset>
                    <legend><em>Datos para el Cálculo de incremento de ganancia de potencia</em></legend>
                    <br>
                    <div class="row">	
                        <label for="nombreCircuito">Nombre de circuito: &nbsp;</label>
                        <input type = "text" id="nombreCircuito" name = "nombreCircuito" maxlength="30" required>
                    </div>                    
                    <div class="row">	
                        <label for="paisCircuito">País: &nbsp;</label>
                        <input type = "text" id="paisCircuito" name = "paisCircuito" maxlength="20" required>
                    </div>                    
                    <div class="row">	
                        <label for="ciudadCircuito">Ciudad: &nbsp;</label>
                        <input type = "text" id="ciudadCircuito" name = "ciudadCircuito" maxlength="20" required>
                    </div>                    
                    <div class="row">	
                        <label for="numeroVueltasCircuito">Número de vueltas: &nbsp;</label>
                        <input type="number" id="numeroVueltasCircuito" name = "numeroVueltasCircuito" min="40" max="80" required>
                    </div>                    
                    <div class="row">	
                        <label for="longitudVuelta">Longitud de vuelta: &nbsp;</label>
                        <input type="number" id="longitudVuelta" name = "longitudVuelta" step="0.01" min = "3000" max="9000" required>
                    </div>                    
                    <div class="row">	
                        <label for="numeroCurvasVuelta">Curvas por vuelta: &nbsp;</label>
                        <input type="number" id="numeroCurvasVuelta" name = "numeroCurvasVuelta" min = "6" max="20" required>
                    </div>                  
                    <br>
                    <div class="row">
                        <label for="cochesDeEscuderia">Elige escudería: &nbsp;</label>
                        <select id="cochesDeEscuderia" name="cochesDeEscuderia" required>
                            <sql:setDataSource driver="com.mysql.cj.jdbc.Driver" 
                                               url= "jdbc:mysql://localhost:3305/formula1_review" user= "usuario" password= "clave" />
                            <sql:query var="consultaEscuderias" sql="SELECT * FROM formula1_review.escuderia" ></sql:query>
                            <c:forEach var= "escuderia" items="${consultaEscuderias.rows}">
                                <optgroup label="${escuderia.NOMBRE_ESCUDERIA})"></optgroup>
                                <sql:query var="consultaCochesEscuderia" sql="SELECT * FROM formula1_review.coche WHERE ID_ESCUDERIA = ?" >
                                    <sql:param value="${escuderia.ID_ESCUDERIA}" />
                                </sql:query>
                                <c:forEach var= "coche" items="${consultaCochesEscuderia.rows}">
                                    <option value ="${coche.ID_COCHE}">${coche.MODELO_COCHE}</option>
                                </c:forEach>
                            </c:forEach>
                        </select>
                    </div>
                </fieldset>
                <div class= "row">
                    <input type="submit" value="Enviar"/>
                </div>
            </form>
            <br><br>
            <%@include file="/WEB-INF/jspf/footer.jspf" %>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>
</html>
