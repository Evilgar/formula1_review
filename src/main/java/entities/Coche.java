package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "coche")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Coche.findAll", query = "SELECT c FROM Coche c"),
    @NamedQuery(name = "Coche.findByIdCoche", query = "SELECT c FROM Coche c WHERE c.idCoche = :idCoche"),
    @NamedQuery(name = "Coche.findByModeloCoche", query = "SELECT c FROM Coche c WHERE c.modeloCoche = :modeloCoche"),
    @NamedQuery(name = "Coche.findByGananciaPotenciaKwPorCurva", query = "SELECT c FROM Coche c WHERE c.gananciaPotenciaKwPorCurva = :gananciaPotenciaKwPorCurva")})
public class Coche implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_COCHE", nullable = false)
    private Integer idCoche;
    @Size(max = 20)
    @Column(name = "MODELO_COCHE", length = 20)
    private String modeloCoche;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "GANANCIA_POTENCIA_KW_POR_CURVA", precision = 12, scale = 0)
    private Float gananciaPotenciaKwPorCurva;
    @ManyToOne
    @JoinColumn(name = "ID_ESCUDERIA")
    private Escuderia escuderia;

    public Coche() {
    }

    public Coche(Integer idCoche) {
        this.idCoche = idCoche;
    }

    public Integer getIdCoche() {
        return idCoche;
    }

    public Coche(String modeloCoche, Float gananciaPotenciaKwPorCurva, Escuderia escuderia) {
        this.modeloCoche = modeloCoche;
        this.gananciaPotenciaKwPorCurva = gananciaPotenciaKwPorCurva;
        this.escuderia = escuderia;
    }

    public void setIdCoche(Integer idCoche) {
        this.idCoche = idCoche;
    }

    public String getModeloCoche() {
        return modeloCoche;
    }

    public void setModeloCoche(String modeloCoche) {
        this.modeloCoche = modeloCoche;
    }

    public Float getGananciaPotenciaKwPorCurva() {
        return gananciaPotenciaKwPorCurva;
    }

    public void setGananciaPotenciaKwPorCurva(Float gananciaPotenciaKwPorCurva) {
        this.gananciaPotenciaKwPorCurva = gananciaPotenciaKwPorCurva;
    }

    public Escuderia getIdEscuderia() {
        return escuderia;
    }

    public void setIdEscuderia(Escuderia escuderia) {
        this.escuderia = escuderia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCoche != null ? idCoche.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Coche)) {
            return false;
        }
        Coche other = (Coche) object;
        if ((this.idCoche == null && other.idCoche != null) || (this.idCoche != null && !this.idCoche.equals(other.idCoche))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Coche[ idCoche=" + idCoche + " ]";
    }

}
