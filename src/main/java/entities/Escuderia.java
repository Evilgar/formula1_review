package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author eva
 */
@Entity
@Table(name = "escuderia", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"NOMBRE_ESCUDERIA"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Escuderia.findAll", query = "SELECT e FROM Escuderia e"),
    @NamedQuery(name = "Escuderia.findByIdEscuderia", query = "SELECT e FROM Escuderia e WHERE e.idEscuderia = :idEscuderia"),
    @NamedQuery(name = "Escuderia.findByNombreEscuderia", query = "SELECT e FROM Escuderia e WHERE e.nombreEscuderia = :nombreEscuderia")})
public class Escuderia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ESCUDERIA", nullable = false)
    private Integer idEscuderia;
    @Size(max = 50)
    @Column(name = "NOMBRE_ESCUDERIA", length = 50)
    private String nombreEscuderia;
    @OneToMany(mappedBy = "escuderia")
    private Collection<Coche> cocheCollection;

    public Escuderia() {
    }

    public Escuderia(Integer idEscuderia) {
        this.idEscuderia = idEscuderia;
    }

    public Integer getIdEscuderia() {
        return idEscuderia;
    }

    public void setIdEscuderia(Integer idEscuderia) {
        this.idEscuderia = idEscuderia;
    }

    public String getNombreEscuderia() {
        return nombreEscuderia;
    }

    public void setNombreEscuderia(String nombreEscuderia) {
        this.nombreEscuderia = nombreEscuderia;
    }

    public Collection<Coche> getCocheCollection() {
        return cocheCollection;
    }

    public void setCocheCollection(Collection<Coche> cocheCollection) {
        this.cocheCollection = cocheCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEscuderia != null ? idEscuderia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Escuderia)) {
            return false;
        }
        Escuderia other = (Escuderia) object;
        if ((this.idEscuderia == null && other.idEscuderia != null) || (this.idEscuderia != null && !this.idEscuderia.equals(other.idEscuderia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Escuderia[ idEscuderia=" + idEscuderia + " ]";
    }
    
}
