package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author eva
 */
@Entity
@Table(name = "circuito", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"NOMBRE_CIRCUITO"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Circuito.findAll", query = "SELECT c FROM Circuito c"),
    @NamedQuery(name = "Circuito.findByIdCircuito", query = "SELECT c FROM Circuito c WHERE c.idCircuito = :idCircuito"),
    @NamedQuery(name = "Circuito.findByNombreCircuito", query = "SELECT c FROM Circuito c WHERE c.nombreCircuito = :nombreCircuito"),
    @NamedQuery(name = "Circuito.findByPaisCircuito", query = "SELECT c FROM Circuito c WHERE c.paisCircuito = :paisCircuito"),
    @NamedQuery(name = "Circuito.findByCiudadCircuito", query = "SELECT c FROM Circuito c WHERE c.ciudadCircuito = :ciudadCircuito"),
    @NamedQuery(name = "Circuito.findByNumeroVueltasCircuito", query = "SELECT c FROM Circuito c WHERE c.numeroVueltasCircuito = :numeroVueltasCircuito"),
    @NamedQuery(name = "Circuito.findByLongitudVuelta", query = "SELECT c FROM Circuito c WHERE c.longitudVuelta = :longitudVuelta"),
    @NamedQuery(name = "Circuito.findByNumeroCurvasVuelta", query = "SELECT c FROM Circuito c WHERE c.numeroCurvasVuelta = :numeroCurvasVuelta")})
public class Circuito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_CIRCUITO", nullable = false)
    private Integer idCircuito;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NOMBRE_CIRCUITO", nullable = false, length = 30)
    private String nombreCircuito;
    @Size(max = 20)
    @Column(name = "PAIS_CIRCUITO", length = 20)
    private String paisCircuito;
    @Size(max = 20)
    @Column(name = "CIUDAD_CIRCUITO", length = 20)
    private String ciudadCircuito;
    @Column(name = "NUMERO_VUELTAS_CIRCUITO")
    private Integer numeroVueltasCircuito;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "LONGITUD_VUELTA", precision = 12, scale = 0)
    private Float longitudVuelta;
    @Column(name = "NUMERO_CURVAS_VUELTA")
    private Integer numeroCurvasVuelta;

    public Circuito() {
    }

    public Circuito(Integer idCircuito) {
        this.idCircuito = idCircuito;
    }

    public Circuito(Integer idCircuito, String nombreCircuito) {
        this.idCircuito = idCircuito;
        this.nombreCircuito = nombreCircuito;
    }

    public Integer getIdCircuito() {
        return idCircuito;
    }

    public Circuito(Integer idCircuito, String nombreCircuito, String paisCircuito, String ciudadCircuito, Integer numeroVueltasCircuito, Float longitudVuelta, Integer numeroCurvasVuelta) {
        this.idCircuito = idCircuito;
        this.nombreCircuito = nombreCircuito;
        this.paisCircuito = paisCircuito;
        this.ciudadCircuito = ciudadCircuito;
        this.numeroVueltasCircuito = numeroVueltasCircuito;
        this.longitudVuelta = longitudVuelta;
        this.numeroCurvasVuelta = numeroCurvasVuelta;
    }

    public void setIdCircuito(Integer idCircuito) {
        this.idCircuito = idCircuito;
    }

    public String getNombreCircuito() {
        return nombreCircuito;
    }

    public void setNombreCircuito(String nombreCircuito) {
        this.nombreCircuito = nombreCircuito;
    }

    public String getPaisCircuito() {
        return paisCircuito;
    }

    public void setPaisCircuito(String paisCircuito) {
        this.paisCircuito = paisCircuito;
    }

    public String getCiudadCircuito() {
        return ciudadCircuito;
    }

    public void setCiudadCircuito(String ciudadCircuito) {
        this.ciudadCircuito = ciudadCircuito;
    }

    public Integer getNumeroVueltasCircuito() {
        return numeroVueltasCircuito;
    }

    public void setNumeroVueltasCircuito(Integer numeroVueltasCircuito) {
        this.numeroVueltasCircuito = numeroVueltasCircuito;
    }

    public Float getLongitudVuelta() {
        return longitudVuelta;
    }

    public void setLongitudVuelta(Float longitudVuelta) {
        this.longitudVuelta = longitudVuelta;
    }

    public Integer getNumeroCurvasVuelta() {
        return numeroCurvasVuelta;
    }

    public void setNumeroCurvasVuelta(Integer numeroCurvasVuelta) {
        this.numeroCurvasVuelta = numeroCurvasVuelta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCircuito != null ? idCircuito.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Circuito)) {
            return false;
        }
        Circuito other = (Circuito) object;
        if ((this.idCircuito == null && other.idCircuito != null) || (this.idCircuito != null && !this.idCircuito.equals(other.idCircuito))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Circuito[ idCircuito=" + idCircuito + " ]";
    }

}
