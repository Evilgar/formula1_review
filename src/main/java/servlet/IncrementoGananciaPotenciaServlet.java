package servlet;

import entities.Circuito;
import entities.Coche;
import entities.Escuderia;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.IServiceCircuito;
import services.IServiceCoche;
import services.IServiceEscuderia;
import services.IServiceIncrementoPotencia;
import services.ServiceCircuito;
import services.ServiceCoche;
import services.ServiceEscuderia;
import services.ServiceIncrementoPotencia;

public class IncrementoGananciaPotenciaServlet extends HttpServlet {

    @Inject
    IServiceCoche serviceCoche;
    @Inject
    IServiceCircuito serviceCircuito;
    @Inject
    IServiceEscuderia serviceEscuderia;
    @Inject
    IServiceIncrementoPotencia serviceCalculo;

    private static final Logger LOGGER = LoggerFactory.getLogger(IncrementoGananciaPotenciaServlet.class);

    public IncrementoGananciaPotenciaServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        procesarPeticion(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        procesarPeticion(request, response);
    }

    protected void procesarPeticion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            String paisCircuito = request.getParameter("paisCircuito");
            String ciudadCircuito = request.getParameter("ciudadCircuito");
            String nombreCircuito = request.getParameter("nombreCircuito");

            int numeroVueltasCircuito, numeroCurvasVuelta, idCoche;
            float longitudVuelta;
            numeroVueltasCircuito = Integer.parseInt(request.getParameter("numeroVueltasCircuito"));
            numeroCurvasVuelta = Integer.parseInt(request.getParameter("numeroCurvasVuelta"));
            idCoche = Integer.parseInt(request.getParameter("cochesDeEscuderia"));
            longitudVuelta = Float.parseFloat(request.getParameter("longitudVuelta"));

            Circuito circuito;
            serviceCircuito = new ServiceCircuito();
            if (!serviceCircuito.existeCircuito(nombreCircuito)) {
                circuito = serviceCircuito.creaCircuito(0, nombreCircuito, paisCircuito,
                        ciudadCircuito, numeroVueltasCircuito, longitudVuelta, numeroCurvasVuelta);
            } else {
                circuito = serviceCircuito.getCircuitoPorNombre(nombreCircuito);
            }
            int idCircuito = circuito.getIdCircuito();
            serviceCalculo = new ServiceIncrementoPotencia();
            float incrementoGananciaCocheSeleccionadoEnCircuitoElegido = serviceCalculo.
                    getIncrementoPotencia(idCoche, idCircuito);
            serviceCoche = new ServiceCoche();
            Coche cocheF1 = serviceCoche.getCochePorId(idCoche);
            String modeloCoche = cocheF1.getModeloCoche();
            float gananciaPotenciaKwPorCurva = cocheF1.getGananciaPotenciaKwPorCurva();
            Integer idEscuderia = cocheF1.getIdEscuderia().getIdEscuderia();
            serviceEscuderia = new ServiceEscuderia();
            Escuderia escuderia = serviceEscuderia.findEscuderiaPorId(idEscuderia);
            String nombreEscuderia = escuderia.getNombreEscuderia();

            String gananciaPotenciaPorCurva = Float.toString(gananciaPotenciaKwPorCurva);
            String gananciaCocheSeleccionadoCircuitoSeleccionado = Float.
                    toString(incrementoGananciaCocheSeleccionadoEnCircuitoElegido);
            request.setAttribute("modeloCoche", modeloCoche);
            request.setAttribute("nombreEscuderia", nombreEscuderia);
            request.setAttribute("nombreCircuito", nombreCircuito);
            request.setAttribute("gananciaPotenciaPorCurva", gananciaPotenciaPorCurva);
            request.setAttribute("gananciaCocheSeleccionadoCircuitoSeleccionado",
                    gananciaCocheSeleccionadoCircuitoSeleccionado);
            RequestDispatcher rd = this.getServletContext()
                    .getRequestDispatcher("/resultado.jsp");
            rd.forward(request, response);

        } catch (Exception e) {
            LOGGER.error("Error. " + e.getLocalizedMessage());
            RequestDispatcher rd = this.getServletContext()
                    .getRequestDispatcher("/error.jsp");
            rd.forward(request, response);
        }
    }
}
