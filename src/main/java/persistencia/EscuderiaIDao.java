/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import entities.Escuderia;
import java.util.List;


/**
 *
 * @author eva
 */
public interface EscuderiaIDao {

    Escuderia find(Integer id);

    List<Escuderia> findAllEscuderias();
}
