/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import entities.Coche;
import entities.Escuderia;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author eva
 */
public class CochesF1DaoImp extends AbstractDao<Coche, Integer> implements CochesF1IDao {

    private static final String ENCUENTRA_POR_IDESCUDERIA = "SELECT * FROM formula1_review.coche WHERE ID_ESCUDERIA = :idEscuderia";

    public CochesF1DaoImp(EntityManager em) {
        super(em);
    }

    @Override
    public Coche find(Integer idCoche) {
        em = this.getEntityManager();
        return (Coche) em.find(Coche.class, idCoche);
    }

    @Override
    public List<Coche> findAllCoches() {
        em = this.getEntityManager();
        return (List<Coche>) em.createNamedQuery("Coche.findAll", Coche.class).getResultList();
    }

    @Override
    public List<Coche> findCochePorEscuderia(Escuderia escuderia) {
        int idEscuderia = escuderia.getIdEscuderia();
        em = this.getEntityManager();
        return (List<Coche>) em.createNamedQuery(ENCUENTRA_POR_IDESCUDERIA, Coche.class)
                .setParameter("idEscuderia", idEscuderia).getSingleResult();
    }

}
