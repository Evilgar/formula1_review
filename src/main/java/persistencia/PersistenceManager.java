/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

/**
 *
 * @author eva
 */
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceManager {

    static private final String PERSISTENCE_UNIT_NAME = "my_persistence_unit";
    protected static PersistenceManager pm = null;
    protected EntityManagerFactory emf = null;

    private PersistenceManager() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
            this.setEntityManagerFactory(emf);
        }
    }

    public static PersistenceManager getInstance() {
        if (pm == null) {
            pm = new PersistenceManager();
        }
        return pm;
    }

    private void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return this.emf;
    }

    public EntityManager createEntityManager() {
        return emf.createEntityManager();
    }
}
