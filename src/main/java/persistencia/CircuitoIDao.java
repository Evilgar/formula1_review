/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import entities.Circuito;
import java.util.List;

/**
 *
 * @author eva
 */
public interface CircuitoIDao {

    Circuito create(Circuito t);

    Circuito find(Integer id);

    List<Circuito> findAllCircuitos();
    
    Circuito findCircuitoPorNombre(String nombreCircuito);
}
