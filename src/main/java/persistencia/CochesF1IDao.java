/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import entities.Coche;
import entities.Escuderia;
import java.util.List;

/**
 *
 * @author eva
 */

public interface CochesF1IDao {


    Coche find(Integer id);
   
    List<Coche> findAllCoches();
    
    List<Coche> findCochePorEscuderia(Escuderia idEscuderia);
    
}
