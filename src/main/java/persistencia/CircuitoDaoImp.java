/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import entities.Circuito;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author eva
 */
public class CircuitoDaoImp extends AbstractDao<Circuito, Integer> implements CircuitoIDao {

    private static final String FIND_ALL_CIRCUITOS_ORDENED = "SELECT c FROM formula1.circuito c ORDER BY ID_CIRCUITO ASC";

    public CircuitoDaoImp(EntityManager em) {
        super(em);
    }

    @Override
    public Circuito findCircuitoPorNombre(String nombreCircuito) {
        em = this.getEntityManager();
        return (Circuito) em.createNamedQuery("Circuito.findByNombreCircuito", Circuito.class)
                .setParameter("nombreCircuito", nombreCircuito).getSingleResult();
    }

    @Override
    public List<Circuito> findAllCircuitos() {
        em = this.getEntityManager();
        return em.createNamedQuery("Circuito.findAll", Circuito.class).getResultList();
    }

    @Override
    public Circuito create(Circuito t) {
        return super.create(t);
    }

    @Override
    public void delete(Circuito t) {
        super.delete(t);
    }

    @Override
    public Circuito find(Integer id) {
        em = this.getEntityManager();
        return (Circuito) em.find(Circuito.class, id);
    }


    @Override
    public Circuito update(Circuito t) {
        return super.update(t);
    }

}
