package persistencia;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractDao<T, K> {
    
    protected EntityManager em;
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDao.class);

    public AbstractDao(EntityManager em) {
        if (!em.getTransaction().isActive()) {
            LOGGER.error("transacción inactiva");
        }
        this.em = em;
    }

    public EntityManager getEntityManager() {
        return em;
    }

    public T create(T t) {
        try {
            em.persist(t);
            em.flush();
            em.refresh(t);
        } catch (EntityExistsException ex) {
            LOGGER.error("La entidad ya existe", ex);
        }
        return t;
    }

    public T update(T t) {
        return (T) em.merge(t);
    }

    public void delete(T t) {
        t = em.merge(t);
        em.remove(t);
    }

    public abstract T find(K id);
}
