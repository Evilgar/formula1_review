package persistencia;

import entities.Escuderia;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class EscuderiaDaoImp extends AbstractDao<Escuderia, Integer> implements EscuderiaIDao {

    String FIND_ALL_ESCUDERIAS = "SELECT e FROM Escuderia e ";

    public EscuderiaDaoImp(EntityManager em) {
        super(em);
    }

    @Override
    public Escuderia find(Integer id) {
        em = this.getEntityManager();
        return (Escuderia) em.find(Escuderia.class, id);
    }

    @Override
    public List<Escuderia> findAllEscuderias() {
        em = this.getEntityManager();
        TypedQuery<Escuderia> query = em.createQuery("Escuderia.findAll", Escuderia.class);
        return (List<Escuderia>) query.getResultList();
    }
}
