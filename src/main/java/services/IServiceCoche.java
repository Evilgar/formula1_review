/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Coche;
import entities.Escuderia;
import java.util.List;

/**
 *
 * @author eva
 */
public interface IServiceCoche {

    Coche getCochePorId(Integer id);

    List<Coche> getAll();

    List<Coche> getCochePorEscuderia(Escuderia idEscuderia);
    

}
