/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Escuderia;
import java.util.List;
import javax.persistence.EntityManager;
import persistencia.EscuderiaDaoImp;
import persistencia.EscuderiaIDao;
import persistencia.PersistenceManager;

/**
 *
 * @author eva
 */
public class ServiceEscuderia implements IServiceEscuderia {

    @Override
    public List<Escuderia> getAllEscuderia() {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        EscuderiaIDao daoEscuderia = new EscuderiaDaoImp(em);      
        return daoEscuderia.findAllEscuderias();
    }


    @Override
    public Escuderia findEscuderiaPorId(Integer id) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        EscuderiaIDao daoEscuderia = new EscuderiaDaoImp(em);
        return daoEscuderia.find(id);
    }
}
