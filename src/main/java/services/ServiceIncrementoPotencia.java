/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Circuito;
import entities.Coche;
import javax.persistence.EntityManager;
import persistencia.CircuitoDaoImp;
import persistencia.CircuitoIDao;
import persistencia.CochesF1DaoImp;
import persistencia.CochesF1IDao;
import persistencia.PersistenceManager;

/**
 *
 * @author eva
 */
public class ServiceIncrementoPotencia implements IServiceIncrementoPotencia {

    @Override
    public float getIncrementoPotencia(Integer idCoche, Integer idCircuito) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        CochesF1IDao cocheDao = new CochesF1DaoImp(em);
        Coche cocheF1 = cocheDao.find(idCoche);
        float gananciaPotenciaCocheF1 = cocheF1.getGananciaPotenciaKwPorCurva();
        CircuitoIDao circuitoDao = new CircuitoDaoImp(em);
        Circuito circuito = circuitoDao.find(idCircuito);
        int numeroCurvasVuelta = circuito.getNumeroCurvasVuelta();
        float longitudVuelta = circuito.getLongitudVuelta();
        int numeroVueltasCircuito = circuito.getNumeroVueltasCircuito();
        return gananciaPotenciaCocheF1 * numeroCurvasVuelta * longitudVuelta * numeroVueltasCircuito;
    }
}
