/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Circuito;
import java.util.List;

/**
 *
 * @author eva
 */
public interface IServiceCircuito {

    Circuito creaCircuito(Integer idCircuito, String nombreCircuito, String paisCircuito, String ciudadCircuito, int numeroVueltasCircuito, float longitudVuelta, int numeroCurvasVuelta);

    List<Circuito> getAll();

    boolean existeCircuito(String nombreCircuito);
    
    Circuito getCircuitoById(Integer idCircuito);
    
    Circuito getCircuitoPorNombre(String name);    
}
