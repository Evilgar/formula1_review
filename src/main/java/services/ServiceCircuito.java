/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Circuito;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import persistencia.CircuitoDaoImp;
import persistencia.CircuitoIDao;
import persistencia.PersistenceManager;

/**
 *
 * @author eva
 */
public class ServiceCircuito implements IServiceCircuito {

    @Override
    public Circuito creaCircuito(Integer idCircuito, String nombreCircuito, String paisCircuito,
            String ciudadCircuito, int numeroVueltasCircuito, float longitudVuelta, int numeroCurvasVuelta) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        CircuitoIDao daoCircuito = new CircuitoDaoImp(em);
        Circuito circuito = new Circuito(idCircuito, nombreCircuito, paisCircuito, ciudadCircuito, numeroVueltasCircuito, longitudVuelta, numeroCurvasVuelta);
        daoCircuito.create(circuito);
        em.getTransaction().commit();
        em.close();
        return circuito;
    }

    @Override
    public List<Circuito> getAll() {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        CircuitoIDao daoCircuito = new CircuitoDaoImp(em);
        List<Circuito> list = daoCircuito.findAllCircuitos();
        System.out.println(list.size());
        return list;
    }

    @Override
    public Circuito getCircuitoPorNombre(String name) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        CircuitoIDao daoCircuito = new CircuitoDaoImp(em);
        return daoCircuito.findCircuitoPorNombre(name);
    }

    @Override
    public Circuito getCircuitoById(Integer idCircuito) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        CircuitoIDao daoCircuito = new CircuitoDaoImp(em);
        return daoCircuito.find(idCircuito);
    }

    @Override
    public boolean existeCircuito(String nombreCircuito) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
        em.getTransaction().begin();
        boolean existe = false;
        String nombCircuito;
        try {

            Iterator<Circuito> it = this.getAll().iterator();
            while (it.hasNext()) {
                nombCircuito = it.next().getNombreCircuito();
                if (nombCircuito.equals(nombreCircuito)) {
                    existe = true;
                }
            }
        } catch (Exception e) {
            System.out.println("No lee de la tabla");
            System.out.println("El error es: " + e.getMessage());
        }
        return existe;
    }
}
