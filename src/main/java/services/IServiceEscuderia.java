package services;

import entities.Escuderia;
import java.util.List;

public interface IServiceEscuderia {

    List<Escuderia> getAllEscuderia();
    
    Escuderia findEscuderiaPorId(Integer id);  
}
