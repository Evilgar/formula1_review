/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Coche;
import entities.Escuderia;
import java.util.List;
import javax.persistence.EntityManager;
import persistencia.CochesF1DaoImp;
import persistencia.CochesF1IDao;
import persistencia.PersistenceManager;

public class ServiceCoche implements IServiceCoche {

    @Override
    public List<Coche> getAll() {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
                em.getTransaction().begin();
        CochesF1IDao daocoche = new CochesF1DaoImp(em);
        return daocoche.findAllCoches();
    }

    @Override
    public Coche getCochePorId(Integer id) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
                em.getTransaction().begin();
        CochesF1IDao daocoche = new CochesF1DaoImp(em);
        return daocoche.find(id);
    }

    @Override
    public List<Coche> getCochePorEscuderia(Escuderia idEscuderia) {
        EntityManager em = PersistenceManager.getInstance().createEntityManager();
                em.getTransaction().begin();
        CochesF1IDao daocoche = new CochesF1DaoImp(em);
        return daocoche.findCochePorEscuderia(idEscuderia);
    }
}
